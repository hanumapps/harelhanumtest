package app.com.harelhanumtest.data

class ImageModel(val largeImageURL: String, val imageWidth: Int, val imageHeight: Int)