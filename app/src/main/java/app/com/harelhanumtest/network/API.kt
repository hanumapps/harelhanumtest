package com.pay.scs.network

import app.com.harelhanumtest.network.ApiClient
import app.com.harelhanumtest.network.models.SearchImagesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface API {

    @GET("api/")
    fun searchImages(
        @Query("q") q: String,
        @Query("key") key: String = ApiClient.API_KEY
    ): Observable<SearchImagesResponse>

    @GET("api/")
    fun searchImagesForPage(
        @Query("q") q: String,
        @Query("page") page: String,
        @Query("key") key: String = ApiClient.API_KEY
    ): Observable<SearchImagesResponse>


}
