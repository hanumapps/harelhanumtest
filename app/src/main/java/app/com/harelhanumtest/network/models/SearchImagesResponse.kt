package app.com.harelhanumtest.network.models

import app.com.harelhanumtest.data.ImageModel

class SearchImagesResponse(val hits: ArrayList<ImageModel>)