package app.com.harelhanumtest.network

import app.com.harelhanumtest.BuildConfig
import com.pay.scs.network.API
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
    companion object {
        private var retrofit: Retrofit? = null
        private var service: API? = null
        private var timeout: Long = 30
        private val baseUrl = "https://pixabay.com/"
        val API_KEY = "13324593-a01f72c199949cb9d919f5e8e"

        fun service(): API? {
            if (retrofit == null) {

                val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

                val okHttpClient = OkHttpClient.Builder()
                    .cache(null)
                    .connectTimeout(timeout, TimeUnit.SECONDS)
                    .writeTimeout(timeout, TimeUnit.SECONDS)
                    .readTimeout(timeout, TimeUnit.SECONDS)

                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor()
                    logging.level = HttpLoggingInterceptor.Level.BODY
                    okHttpClient.addInterceptor(logging)
                }

                retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okHttpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addCallAdapterFactory(rxAdapter)
                    .build()
            }

            if (service == null) {
                service = retrofit?.create<API>(API::class.java)
            }

            return service
        }

    }
}
