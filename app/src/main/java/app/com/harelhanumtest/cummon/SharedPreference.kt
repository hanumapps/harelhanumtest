package app.com.harelhanumtest.cummon

import android.app.Activity
import android.content.Context

class SharedPreference {

    companion object {

        fun save(activity: Activity, key: String, value: String) {
            val editor = activity.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE).edit()
            editor.remove(key)
            editor.putString(key, value)
            editor.apply()
        }

        fun get(activity: Activity, key: String, value: String?): String? =
            activity.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE).getString(key, value)

    }

}