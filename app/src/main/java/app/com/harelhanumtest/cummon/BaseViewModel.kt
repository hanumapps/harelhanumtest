package app.com.harelhanumtest.cummon

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {
    val disposables = CompositeDisposable()
    val isLoading = MutableLiveData<Boolean>(false)
}