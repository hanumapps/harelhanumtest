package app.com.harelhanumtest.cummon

class Constants {

    companion object {
        const val GRID_COLOUMNS_SPACE = 60
        const val SHARED_PREFERENCES = "SHARED_PREFERENCES"
        const val LAST_SEARCH = "LAST_SEARCH"
        const val POSITION = "POSITION"
        const val IMAGES = "IMAGES"
    }

}