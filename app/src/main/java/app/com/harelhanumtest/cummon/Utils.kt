package app.com.harelhanumtest.cummon

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import java.io.ByteArrayOutputStream


fun Context.getScreenSize(): Pair<Float, Float> {
    val displayMetrics = resources.displayMetrics
    val dpHeight = displayMetrics.heightPixels / displayMetrics.density
    val dpWidth = displayMetrics.widthPixels / displayMetrics.density
    return Pair(dpHeight, dpWidth)
}

fun Context.convertPxToDp(px: Float): Int {
    val scale = resources.displayMetrics.density
    return (px * scale).toInt()
}

fun Bitmap.toUri(context: Context) : Uri {
    val bytes = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val path = MediaStore.Images.Media.insertImage(context.contentResolver, this, "Title", null)
    return Uri.parse(path)
}