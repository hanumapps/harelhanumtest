package app.com.harelhanumtest.presentation.home_screen

import androidx.lifecycle.MutableLiveData
import app.com.harelhanumtest.cummon.BaseViewModel
import app.com.harelhanumtest.data.ImageModel
import app.com.harelhanumtest.network.ApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class ImagesGridViewModel : BaseViewModel() {

    enum class ImagesGridResult {
        NONE, SEARCH_SUCCESS, SEARCH_FOR_PAGE_SUCCESS, FAILED
    }

    val result = MutableLiveData(ImagesGridResult.NONE)
    val images = arrayListOf<ImageModel>()
    var lastSearch: String? = null
    var page: Int = 1 // default


    fun serchForImages(searchText: String) {
        isLoading.value = true
        ApiClient.service()?.searchImages(searchText)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(Schedulers.io())
            ?.subscribeBy(
                onNext = {
                    lastSearch = searchText
                    isLoading.value = false
                    images.clear()
                    images.addAll(it.hits)
                    result.value = ImagesGridResult.SEARCH_SUCCESS
                },
                onError = {
                    isLoading.value = false
                    result.value = ImagesGridResult.FAILED
                }
            )?.addTo(disposables)
    }

    fun loadNextPage() {
        page += 1
        isLoading.value = true
        lastSearch?.let {
            ApiClient.service()?.searchImagesForPage(it, page.toString())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())
                ?.subscribeBy(
                    onNext = {
                        isLoading.value = false
                        images.clear()
                        images.addAll(it.hits)
                        result.value = ImagesGridResult.SEARCH_FOR_PAGE_SUCCESS
                    },
                    onError = {
                        isLoading.value = false
                        result.value = ImagesGridResult.FAILED
                    }
                )?.addTo(disposables)
        }
    }
}