package app.com.harelhanumtest.presentation.home_screen

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import app.com.harelhanumtest.R
import app.com.harelhanumtest.cummon.Constants
import app.com.harelhanumtest.cummon.convertPxToDp
import app.com.harelhanumtest.cummon.getScreenSize
import app.com.harelhanumtest.data.ImageModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.image_cell.view.*


class ImagesAdapter(val delegate: ImagesAdapterDelegate) : BaseAdapter() {

    interface ImagesAdapterDelegate {
        fun onLastPosition()
    }

    val images: ArrayList<ImageModel> = arrayListOf()

    override fun getCount(): Int {
        return images.size
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val imageItem = images[position]
        var view = convertView

        if (view == null) {
            val layoutInflater = LayoutInflater.from(parent.context)
            view = layoutInflater.inflate(R.layout.image_cell, null)
        }

        val fixedHeight = getFixedHeight(parent.context)
        val proportionalWidth = parent.context.convertPxToDp(
            getProportionalWidth(imageItem.imageHeight, imageItem.imageWidth, fixedHeight)
        )

        val layoutParams =
            LinearLayout.LayoutParams(
                proportionalWidth,
                parent.context.convertPxToDp(fixedHeight)
            )

        view?.image_view?.layoutParams = layoutParams
        Picasso.get().load(imageItem.largeImageURL).into(view?.image_view, object : Callback {
            override fun onSuccess() {
                Log.d("ImagesAdapter Picasso", "onSuccess")
            }

            override fun onError(e: Exception?) {
                view?.image_view?.setImageResource(R.drawable.no_image)
            }

        })

        if (position == images.size - 1) {
            delegate.onLastPosition()
        }

        return view!!
    }

    private fun getProportionalWidth(originalHeight: Int, originalWidth: Int, fixedHeight: Float): Float {
        val ratio = fixedHeight / originalHeight.toFloat()
        return (originalWidth * ratio)
    }

    private fun getFixedHeight(context: Context): Float {
        val screenSizePair = context.getScreenSize()
        return (screenSizePair.first - Constants.GRID_COLOUMNS_SPACE) / 7
    }

    fun addData(images: ArrayList<ImageModel>) {
        this.images.addAll(images)
        notifyDataSetChanged()
    }

    fun initData(images: ArrayList<ImageModel>) {
        this.images.clear()
        this.images.addAll(images)
        notifyDataSetChanged()
    }

    fun getData() : ArrayList<ImageModel> = images

}