package app.com.harelhanumtest.presentation.gallery

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import app.com.harelhanumtest.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class ViewPagerAdapter(val images: ArrayList<String>) : PagerAdapter() {

    override fun getCount(): Int {
        return this.images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as ConstraintLayout
    }

    override fun instantiateItem(parent: ViewGroup, position: Int): Any {
        val fullScreenImage: ImageView
        val imageNumber: TextView
        val layoutInflater = (parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as? LayoutInflater)
        val view = layoutInflater?.inflate(R.layout.fullscreen_image, parent, false)

        fullScreenImage = view?.findViewById(R.id.full_screen_image) as ImageView
        imageNumber = view.findViewById(R.id.tv_image_number) as TextView

        imageNumber.text = parent.context.resources.getString(R.string.image_number, (position + 1).toString())

        Picasso.get().load(images[position]).into(fullScreenImage, object : Callback {
            override fun onSuccess() {
                Log.d("ViewPagerAdapter", "load image success")
            }

            override fun onError(e: Exception?) {
                fullScreenImage.setImageResource(R.drawable.no_image)
            }

        })


        (parent as ViewPager).addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as ConstraintLayout)
    }
}
