package app.com.harelhanumtest.presentation.gallery


import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.viewpager.widget.ViewPager
import app.com.harelhanumtest.R
import app.com.harelhanumtest.cummon.Constants
import app.com.harelhanumtest.cummon.toUri
import app.com.harelhanumtest.presentation.BaseFragment
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_gallery.*


class GalleryFragment : BaseFragment() {

    var currentPosition: Int = 0
    val images: ArrayList<String> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(toolbar, findNavController())
        btn_share.visibility = View.VISIBLE
        btn_share.setOnClickListener {
            askForPermissions(arrayListOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                , object : BaseMultiplePermissionsListener() {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        super.onPermissionsChecked(report)
                        createBitmapFromUrl()
                    }
                })
        }

        currentPosition = arguments?.getInt(Constants.POSITION) ?: 0
        (arguments?.get(Constants.IMAGES) as? Array<String>)?.let {
            images.addAll(it)
        } ?: run {
            Toast.makeText(context, R.string.defaultError, Toast.LENGTH_SHORT).show()
            findNavController().popBackStack(R.id.imagesGridFragment, false)
        }
        view_pager.adapter = ViewPagerAdapter(images)
        view_pager.setCurrentItem(currentPosition, true)

        view_pager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                currentPosition = position
            }
        })

    }

    private fun createBitmapFromUrl() {
        Picasso.get().load(images[currentPosition]).into(object : com.squareup.picasso.Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                context?.let {
                    bitmap?.toUri(it)?.let { uri ->
                        shareImage(uri)
                    } ?: displayError()
                } ?: displayError()
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                displayError()
            }
        })
    }

    private fun shareImage(uri: Uri) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "image/*"
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)

        context?.startActivities(arrayOf(Intent.createChooser(sharingIntent, resources.getString(R.string.share_with))))
    }

    private fun displayError() {
        Toast.makeText(context, R.string.defaultError, Toast.LENGTH_SHORT).show()
    }

}
