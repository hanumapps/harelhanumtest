package app.com.harelhanumtest.presentation.home_screen


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import app.com.harelhanumtest.R
import app.com.harelhanumtest.cummon.Constants
import app.com.harelhanumtest.cummon.SharedPreference
import app.com.harelhanumtest.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_images_grid.*


class ImagesGridFragment : BaseFragment(), ImagesAdapter.ImagesAdapterDelegate {

    lateinit var viewModel: ImagesGridViewModel
    var adapter: ImagesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_images_grid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(toolbar, findNavController())

        viewModel = ViewModelProvider(this).get(ImagesGridViewModel::class.java)
        initViews()
        bind()
    }

    private fun initViews() {
        activity?.let {
            ET_search.setText(SharedPreference.get(it, Constants.LAST_SEARCH, ""))
        }

        adapter = context?.let { ImagesAdapter(this) }
        grid_view.adapter = adapter

        grid_view.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            adapter?.getData()?.let { images ->
                val bundle = Bundle()
                bundle.putInt(Constants.POSITION, position)
                bundle.putStringArray(Constants.IMAGES, images.map { it.largeImageURL }.toTypedArray())
                findNavController().navigate(R.id.action_imagesGridFragment_to_galleryFragment, bundle)
            }
        }

    }

    private fun bind() {
        btn_go.setOnClickListener {
            viewModel.serchForImages(ET_search.text.toString())
            activity?.let {
                SharedPreference.save(it, Constants.LAST_SEARCH, ET_search.text.toString())
            }
        }

        viewModel.isLoading.observe(this, Observer { state ->
            progress_bar.visibility = if (state) View.VISIBLE else View.GONE
            btn_go.isEnabled = !state
        })

        viewModel.result.observe(this, Observer {
            when (it) {
                ImagesGridViewModel.ImagesGridResult.SEARCH_SUCCESS -> {
                    adapter?.initData(viewModel.images)
                }

                ImagesGridViewModel.ImagesGridResult.SEARCH_FOR_PAGE_SUCCESS -> {
                    adapter?.addData(viewModel.images)
                }

                ImagesGridViewModel.ImagesGridResult.FAILED -> {
                    Toast.makeText(context, R.string.defaultError, Toast.LENGTH_SHORT).show()
                }
            }
        })

    }

    override fun onLastPosition() {
        viewModel.loadNextPage()
    }

}
