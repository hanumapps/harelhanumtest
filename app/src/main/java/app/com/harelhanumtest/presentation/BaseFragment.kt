package app.com.harelhanumtest.presentation

import android.widget.Toast
import androidx.fragment.app.Fragment
import app.com.harelhanumtest.R
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

open class BaseFragment : Fragment() {

    fun askForPermissions(permissons: ArrayList<String>, callBack: BaseMultiplePermissionsListener?) {
        Dexter.withActivity(activity)
            .withPermissions(permissons)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report?.areAllPermissionsGranted() == true) {
                        callBack?.onPermissionsChecked(report)
                    }

                    if (report?.isAnyPermissionPermanentlyDenied == true) {
                        Toast.makeText(context, R.string.permissionPermanentlyDeniedError, Toast.LENGTH_SHORT).show()
                    }
                }
            }).check()
    }

}